const {User} = require ('../models')
const bcrypt = require('bcrypt')
const passport = require('passport')
const session = require('express-session')

module.exports = {
    login : (req,res) =>{
        res.render('login')
    },
    doLogin : (req,res)=>{
        const {username,password} = req.body 
        passport.authenticate('userLocal',{ 
            successRedirect :'/home',
            failureRedirect:'/login',
            failureFlash:true
            
        })
        (req,res);
    },
  
    register : (req,res) =>{
        res.render('register')
    },
    
    doRegister : (req,res)=>{
        const {username, password } = req.body
        const passwordEncrypted = bcrypt.hashSync(password, 10)
        User.create({
            username:username,
            password:passwordEncrypted
        }).then(
            res.render('login')
        )

        
    },
    home :(req,res)=>{
        res.render ('home')
    },
    logout : (req,res)=>{
        req.logout()
        res.redirect('/login')
    },
}