const router = require('express').Router();

const userController = require('../controllers/userController')

const Authenticated = (req,res,next)=>{
    if (req.isAuthenticated() ){
        next()
    }else{
        res.redirect('/login')
    }
}

router.get('/login', userController.login )
router.post('/login', userController.doLogin )

router.get('/register', userController.register )
router.post('/register', userController.doRegister )

router.get('/home', Authenticated,userController.home )

router.get('/logout',Authenticated, userController.logout )

module.exports = router 

